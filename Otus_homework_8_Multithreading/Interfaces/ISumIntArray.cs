﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus_homework_8_Multithreading.Interfaces
{
    public interface ISumIntArray
    {
        public void SumRegular();
        public void SumParallel();
        public void SumWithLINQ();

    }
}
