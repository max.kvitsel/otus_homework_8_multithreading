﻿#### System
| <!-- -->    | <!-- -->    |
--- | --- 
CPU | Intel i7 4770K Cores: 4 Logical processors: 8
Ram | 16 GB 
OS | Windows 10 x64        

#### Results

Array length | Method | Time, ms | Sum
--- | --- | --- | ---
100000 | Regular sum | 1 | 4985669
100000 | Multithread | 4 | 4985669
100000 | LINQ | 39 | 4985669
1000000 | Regular sum | 6 | 50015875
1000000 | Multithread | 26 | 50015875
1000000 | LINQ | 1 | 50015875
10000000 | Regular sum | 58 | 500007797
10000000 | Multithread | 202 | 500007797
10000000 | LINQ | 20 | 500007797