﻿using Otus_homework_8_Multithreading.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus_homework_8_Multithreading
{
    public class SumIntArray : ISumIntArray
    {
        int _arrayLength;
        Random _random;
        Stopwatch _stopwatch;
        int[] _mainArray;
        long _sum;
        int _thread;
        Task<int>[] _taskArray;

        public SumIntArray(int arrayLength)
        {
            _arrayLength = arrayLength;
            _random = new Random();
            _stopwatch = new Stopwatch();
            _mainArray = new int[_arrayLength];
            _thread = Environment.ProcessorCount;
            for (int i = 0; i < _arrayLength; i++)
            {
                _mainArray[i] = _random.Next(1, 100);
            }
        }

        public void SumRegular()
        {
            _stopwatch.Restart();
            _sum = _mainArray.Sum();
            _stopwatch.Stop();
            WriteResult("Regular sum");
        }

        public void SumParallel()
        {
            _stopwatch.Restart();
            IEnumerable<int[]> chanks = _mainArray.Chunk(_mainArray.Length / _thread);
            _taskArray = new Task<int>[chanks.Count()];
            int i = 0;
            foreach (int[] chank in chanks)
            {
                _taskArray[i] = Task.Run(() => chank.Sum());
                ++i;
            }
            Task.WaitAll(_taskArray);
            _sum = _taskArray.Sum(t => t.Result);
            _stopwatch.Stop();
            WriteResult($"Multithread");
        }

        public void SumWithLINQ()
        {
            _stopwatch.Restart();
            _sum = _mainArray.AsParallel().WithDegreeOfParallelism(_thread).Sum();
            _stopwatch.Stop();
            WriteResult("LINQ");
        }

        private void WriteResult(string name)
        {
            Console.WriteLine($"{_arrayLength} | {name} | {_stopwatch.ElapsedMilliseconds} | {_sum}");
        }

    }
}
