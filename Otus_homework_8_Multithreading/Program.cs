﻿using Otus_homework_8_Multithreading.Interfaces;
using System;
using System.Collections.Generic;

namespace Otus_homework_8_Multithreading
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ISumIntArray sumArray;

            List<int> run = new List<int>() { 100000, 1000000, 10000000 };

            Console.WriteLine("Array length | Method | Time, ms | Sum ");
            Console.WriteLine("--- | --- | --- | ---");


            foreach (int i in run)
            {
                sumArray = new SumIntArray(i);
                sumArray.SumRegular();
                sumArray.SumParallel();
                sumArray.SumWithLINQ();
            }

            Console.ReadKey();
        }
    }
}
